obj-m		:= 	dhcp_snoop_IP_spoof.o
KDIR		:=	/home/jagadeesh/git/git
PWD			:=	$(shell pwd)
ccflags-y	:=	-Wno-declaration-after-statement 

default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules
install:
	insmod hook.ko
clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
