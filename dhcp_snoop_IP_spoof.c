#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/if_packet.h>
#include <linux/fs.h>
#include <linux/file.h>
#include <linux/init.h>
#include <linux/inet.h>
#include <linux/if_vlan.h>
#include <linux/hashtable.h>
#include <linux/netdevice.h>

#define IPV6 6
#define IPV4 4
#define DHCPS 67 
#define DHCPC 68
#define HASH_TABLE_BITS 8

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ajit V");
MODULE_AUTHOR("Jagadeesh Pagadala");
MODULE_DESCRIPTION("Module to perform DHCP snoop and preventing IP spoofing");

/* Module parameters */
/*  1. What interface to be used (PF_INET or PF_BRIDGE) to listen on for incoming traffic.
 *  use_bridge
 */
bool use_bridge = 0;
module_param(use_bridge, bool, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP );
MODULE_PARM_DESC(use_bridge, " Set to '1' if bridge interfaces are created, if bridge interfaces are not created then set to '0'. Default value is '0'.");

/*  2. If IP spoof packet is found, then action to be taken. 
 *          Monitor Mode or Preventive Mode.
 */
bool preventive_mode = 0;
module_param(preventive_mode, bool, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP );
MODULE_PARM_DESC(preventive_mode, "If set to '1' IP spoofing will work in Preventive Mode. If set to '0' IP spoofing will work in Monitor Mode. Monitor Mode is the default mode of operation.");


/* This enum describes the DHCP packet type*/
typedef enum {
   DHCPDISCOVER =1,
   DHCPOFFER,
   DHCPREQUEST,
   DHCPDECLINE,
   DHCPACK,
   DHCPNAK,
   DHCPRELEASE,
   DHCPINFORM
}dhcp_pkt_type;


char* inet_ntoa(struct in_addr in, char* buf, size_t* rlen);
int dump_list(char *);
void tick_timer(void);

struct timer_list dhcp_timer;
const int delay = 12; /* time interval for updating DHCP lease time (in sec)*/

struct dhcp_struct
{
    uint32_t ip;
    unsigned char dhcp_mac[6]; /*Client MAC*/
    unsigned int lease_time;
    /* For hash table*/
    struct hlist_node dhcp_hash_list;
    /* type is to maintain the entry type. 1--> DHCP snoop and 0--> Static entry*/
    uint8_t type;
};



/* This structure maintains list of trusted interfaces*/
struct trusted_interface_list
{
    unsigned char name[IFNAMSIZ];
    int name_len;
    struct trusted_interface_list *next;
};

struct trusted_interface_list *trusted_if_head = NULL;

/*Hash table declarartion and definition*/
DEFINE_HASHTABLE(dhcp_snoop_table, HASH_TABLE_BITS);

void clean_trusted_intrerfaces(void)
{
    struct trusted_interface_list *ptr;
    struct trusted_interface_list *tmp;

    if(trusted_if_head == NULL)
        return;

    ptr = trusted_if_head;
    while(ptr->next != NULL) {
        tmp = ptr; 
        ptr = ptr->next;
        kfree(tmp);
    }
    trusted_if_head = NULL; 
}

void clean_hash_table(void)
{
    int i = 0;
    struct dhcp_struct *ptr;

    hash_for_each(dhcp_snoop_table, i, ptr, dhcp_hash_list) {
        hash_del(&ptr->dhcp_hash_list);
        kfree(ptr);
    }
        
}


int dump_list(char *buf)
{

    char buff[512]={'\0'};  //Ravi: This buffer can hold only 4 entries, where as the code is not checking for size of buffer and printing all records
 /*
  * Jagadeesh: This buffer(buff) is just used in inet_ntoa() and buf is being used to write data.
  * buf is sysfs provided buffer and can hold max 4K data.
  *
  */
    int i=0;
    struct in_addr saddr;
    int count = 0;
    struct dhcp_struct *ptr;

    hash_for_each(dhcp_snoop_table, i, ptr, dhcp_hash_list) {
        saddr.s_addr = ptr->ip;
        count+=sprintf(buf+count,"IP address  		-	%s\n",inet_ntoa(saddr,buff,NULL));
        count+=sprintf(buf+count,"MAC address (DHCP) 	-	%02x:%02x:%02x:%02x:%02x:%02x\n",ptr->dhcp_mac[0],ptr->dhcp_mac[1],ptr->dhcp_mac[2],ptr->dhcp_mac[3],ptr->dhcp_mac[4],ptr->dhcp_mac[5]);
        count+=sprintf(buf+count,"Lease Time  		-	%d Seconds\n",ptr->lease_time);
        count+=sprintf(buf+count,"\n\n");
    }
    return count;
}

/*
 * This function will get invoked  for every 30 sec.
 * decreament lease_time by 'delay' and update same
 *
 */
int dhcp_timer_task(int action)
{

    struct dhcp_struct *ptr;
    int i=0;

    /* Iterate through HASH table and update lease time*/
    hash_for_each(dhcp_snoop_table, i, ptr, dhcp_hash_list) {
        /*If type is "0"-----static binding so dont decreament lease time */
        if(ptr->type == 0)
            continue;
	    if(ptr->lease_time < delay) {
	    	ptr->lease_time=0;
            /*delete the entry */
        }
	    else {
	        ptr->lease_time-= delay;  
        }
    }
    return 0;
}

char* inet_ntoa(struct in_addr in, char* buf, size_t* rlen)
{
    int i;
    char* bp;

    /*
     * This implementation is fast because it avoids sprintf(),
     * division/modulo, and global static array lookups.
     */

    bp = buf;
    for (i = 0;i < 4; i++ ) {
        unsigned int o, n;
        o = ((unsigned char*)&in)[i];
        n = o;
        if ( n >= 200 ) {
            *bp++ = '2';
            n -= 200;
        }else if ( n >= 100 ) {
            *bp++ = '1';
            n -= 100;
        }
        if ( o >= 10 ) {
            int i;
            for ( i = 0; n >= 10; i++ ) {
                n -= 10;
            }
            *bp++ = i + '0';
        }
        *bp++ = n + '0';
        *bp++ = '.';
    }
    *--bp = 0;
    if ( rlen ) {
        *rlen = bp - buf;
    }

    return buf;
}
/**
 * function to convert xx.yy.zz.ww dotted decimal to uint32
 */
unsigned int inet_addr(char *str)
{
    int a, b, c, d;
    char arr[4];

    sscanf(str, "%d.%d.%d.%d", &a, &b, &c, &d);
    arr[0] = a; 
    arr[1] = b; 
    arr[2] = c;
    arr[3] = d;

    return *(unsigned int*)arr;
}
/*
 *Function to check if DHCP packet contains Option 51 or not ?
 * Returns 1----> if DHCP packet contains Option 51
 * Returns 0----> if DHCP packet doesn't contain Option 51.
 *
 * Check for yiaddr == 0 and loop in DHCP Options to find out Option 51.
 */

uint32_t dhcp_check_option51(struct sk_buff *skb)
{
    struct iphdr *iph=ip_hdr(skb);
    //struct ethhdr *mh = eth_hdr(skb);
    int offset=0;
    unsigned char *buff=skb->data;//pointer to the start of skb.
    unsigned char op,htype,hlen,hops;//dhcp field.
    uint32_t yiaddr = 0;
    //struct in_addr saddr;
    //char tmp_buffer[512] = {'\0'};
    uint8_t option = 0, length = 0;
    bool dhcp_option51 = 0 ;

    offset+= (int)(iph->ihl)*4;/* 4*ihl will be in bytes, ihl represented in 32 bit words */
    //This is dhcp packet /* Jagadeesh This selection is based on port number 0X43( 67 decimal) */
    offset+=8;//UDP header is 8-byte length
    op=buff[offset];
    htype=buff[offset+1];
    hlen=buff[offset+2];
    hops=buff[offset+3];

    //Message type+Hardware type+Hardware addr length+Hops
    //+Transaction ID+Seconds elapsed+Bootp flags+
    //+Client IP
    offset+=1+1+1+1+4+2+2+4;

    yiaddr = *((uint32_t *)(buff+offset));
    //saddr.s_addr = yiaddr;
    //printk(KERN_DEBUG"%s: yiaddr %s", __func__, inet_ntoa(saddr, tmp_buffer, NULL));
    /* check for existence of Option 51*/
    // Your IP addr + Next server IP(siaddr) + Relay agent IP address(giaddr) + CHADDR + 
    // sname + file + Magic cookie
    offset+=4 + 4 + 4 + 16 + 64 + 128 + 4 ;
    option = *((uint8_t *)(buff+offset));
    // Always Option 53 will be the DHCPACK packet's firist Option
    option = *((uint8_t *)(buff+offset));
    // Option 255 will be Option End
    while(option != 255) {
    // option is one byte(octate) 
        offset += 1;
        length = *((uint8_t *)(buff+offset));
        //printk(KERN_DEBUG"%s: DHCP option %d ", __func__, option);
              
        //  length octate + length  
        offset += 1+length;
        option = *((uint8_t *)(buff+offset));
        if(option == 51){
            dhcp_option51 = 1;
            break;
        }
    }
    /* if option 51 is present and yiaddr !=0. then only update the DHCP snooping table*/
    if((yiaddr != 0) && (dhcp_option51 == 1)){
        return 1;
    }else {
        return 0;
    }

}

/*
 * Function to extract dhcp lease time from DHCP ACK packet
 *
 */
int dhcp_lease_time(struct sk_buff *skb)  //Get DHCP lease time from skb
{
    struct iphdr *iph=ip_hdr(skb);
    int offset,seconds,i;
    unsigned char *buff=skb->data;//pointer to the start of skb.

    offset=0;
    offset+=(int)(iph->ihl)*4;//offset the IP header
    offset+=8;//offset the UDP header

    //Message type+Hardware type+Hardware addr length+Hops
    //+Transaction ID+Seconds elapsed+Bootp flags+
    //+Client IP+Your IP+Next server IP+Relay agent IP
    offset+=1+1+1+1+4+2+2+4+4+4+4;//reference to RFC2131

    //Client MAC address+Server host name+Boot file name+
    //Magic cookie+Option
    offset+=16+64+128+4+3;//reference to RFC2131
    offset+=6+2;
    buff+=offset;
    seconds=0;
    for(i=0;i<4;i++)
        seconds=(seconds*256+*(buff+i));
    return seconds;
}

/*
 * Function to GET IP address allocated from DHCP server.
 */
uint32_t dhcp_ip_address(struct sk_buff *skb)
{

    struct iphdr *iph=ip_hdr(skb);
    int offset;
    unsigned char *buff=skb->data;//pointer to the start of skb.

    offset=0;
    offset+=(int)(iph->ihl)*4;//offset the IP header
    offset+=8;//offset the UDP header

    //Message type+Hardware type+Hardware addr length+Hops
    //+Transaction ID+Seconds elapsed+Bootp flags+
    offset+=1+1+1+1+4+2+2;//reference to RFC2131
    buff+=offset;

    buff+=4;
    return *((uint32_t *)buff);
}

/* This function will LOG the deleted DHCP entry*
 *
 */

void log_dhcp_delete(struct dhcp_struct *dhcp_entry)
{

    struct in_addr saddr;
    char tmp_buffer[512] = {'\0'};

    saddr.s_addr = dhcp_entry->ip;

    printk(KERN_DEBUG"DHCP Snoop module ::DHCP Deleted entry- IP:%s MAC:%02x:%02x:%02x:%02x:%02x:%02x \n", inet_ntoa(saddr, tmp_buffer, NULL), dhcp_entry->dhcp_mac[0], dhcp_entry->dhcp_mac[1], dhcp_entry->dhcp_mac[2], dhcp_entry->dhcp_mac[3], dhcp_entry->dhcp_mac[4], dhcp_entry->dhcp_mac[5]);

}

void log_dhcp_transaction(struct dhcp_struct *dhcpack, dhcp_pkt_type dhcp_type)
{
    struct in_addr saddr;
    char tmp_buffer[512] = {'\0'};


    saddr.s_addr = dhcpack->ip;
    if(dhcp_type == DHCPACK) {
        printk(KERN_DEBUG"DHCP Snoop module:: DHCPACK- IP:%s MAC:%02x:%02x:%02x:%02x:%02x:%02x Lease Time:%d \n", inet_ntoa(saddr, tmp_buffer, NULL), dhcpack->dhcp_mac[0], dhcpack->dhcp_mac[1], dhcpack->dhcp_mac[2], dhcpack->dhcp_mac[3], dhcpack->dhcp_mac[4], dhcpack->dhcp_mac[5], dhcpack->lease_time );
    }else if(dhcp_type == DHCPRELEASE) {
        printk(KERN_DEBUG"DHCP Snoop module:: DHCPRELEASE- IP:%s MAC:%02x:%02x:%02x:%02x:%02x:%02x \n", inet_ntoa(saddr, tmp_buffer, NULL), dhcpack->dhcp_mac[0], dhcpack->dhcp_mac[1], dhcpack->dhcp_mac[2], dhcpack->dhcp_mac[3], dhcpack->dhcp_mac[4], dhcpack->dhcp_mac[5]);
    }else {
        /*Here we can implement other DHCP logs if required in future*/
        printk(KERN_DEBUG"DHCP Snoop module::  DHCP type:%d \n",dhcp_type);
    }

}

/**
 * This function will add to DHCP table, or update an existing entry 
 */
void dhcp_process_packet(struct sk_buff *skb)
{
    int type, i = 0 ;
    struct iphdr *iph=ip_hdr(skb);
    //struct ethhdr *mh = eth_hdr(skb);
    int offset=0;
    unsigned char *buff=skb->data;//pointer to the start of skb.
    unsigned char op,htype,hlen,hops;//dhcp field.
    unsigned char *hw;
    struct dhcp_struct *tmp;
    dhcp_pkt_type dhcp_type = 0;
    int ret=0;

    offset+= (int)(iph->ihl)*4;/* 4*ihl will be in bytes, ihl represented in 32 bit words */

    if(buff[9]==0x11 &&  ((buff[offset]==0x00 && buff[offset+1]==0x43) || (buff[offset+2]==0x00 && buff[offset+3]==0x43))) {
        //This is dhcp packet /* Jagadeesh This selection is based on port number 0X43( 67 decimal) */
        offset+=8;//UDP header is 8-byte length
        op=buff[offset];
        htype=buff[offset+1];
        hlen=buff[offset+2];
        hops=buff[offset+3];

        //Message type+Hardware type+Hardware addr length+Hops
        //+Transaction ID+Seconds elapsed+Bootp flags+
        //+Client IP+Your IP+Next server IP+Relay agent IP
        offset+=1+1+1+1+4+2+2+4+4+4+4;//reference to RFC2131

        hw=buff+offset; /*hw is CHADDR*/
        //Client MAC address+Server host name+Boot file name+
        //Magic cookie+Option
        offset+=16+64+128+4+2;//reference to RFC2131 /*for +4+2 refer RFC2132*/
        type=*(buff+offset);
        if (type == 5) {
            // DHCP ACK pkt type is 5
            /* If DHCP ACK is in responce to the DHCP INFORM, then DHCP ACK packet doesn't
             * contain Lease time and yiaddr.
             * So, check for Option 51 in DHCP ACK packet.
             * 
             * case1: If DHCP packet does not contain Option 51 Just Accept the packet.
             * case2: If DHCP packet contains Option 51, then update the DHCP snoop table. */
            ret = dhcp_check_option51(skb);
            if(ret == 0)
            {
                return; 
            }

            //Search in hash table 
            hash_for_each(dhcp_snoop_table, i, tmp, dhcp_hash_list) {
                // compare stored MAC address with PACKET mac address
                // dhcp_mac is client MAC that is snooped from previous DHCP transaction 
                // hw is CHADDR(client HW addr) field of present DHCP transaction
                if(memcmp(tmp->dhcp_mac, hw, ETH_ALEN) == 0) {
                    // If entry is found, replace the entry (or) update entry
                    tmp->ip = dhcp_ip_address(skb);
                    tmp->lease_time = dhcp_lease_time(skb);
                    tmp->type = 1;
                    /* Log the DHCP ACK*/
                    dhcp_type = DHCPACK;
                    log_dhcp_transaction(tmp, dhcp_type);
                    return;
                }
            }
            // entry is not found, so add to hash table 
            struct dhcp_struct *ptr = kmalloc(sizeof( struct dhcp_struct), GFP_KERNEL);
            if( ptr == NULL ) {
                printk(KERN_ERR"DHCP Snoop module::%s: can't allocate memory\n",__func__);
                return;           
            }
            ptr->ip = dhcp_ip_address(skb);
            memcpy(ptr->dhcp_mac, hw, ETH_ALEN);
            ptr->lease_time = dhcp_lease_time(skb);
            ptr->type = 1;
            // key value is last octate of IP address
            uint8_t key;
            uint32_t ip; 
            ip = dhcp_ip_address(skb);
            key = (uint8_t) ip; /*last octate will be Key used in Hash */
            hash_add(dhcp_snoop_table, &ptr->dhcp_hash_list, key);
            // LOG DHCP ACK
            dhcp_type = DHCPACK;
            log_dhcp_transaction(ptr, dhcp_type);
        }
    }
}

/**
 * This function verifies packet src MAC-IP relation with, existing DHCP snoop table.
 * Also detects Spoofig type like possible spoofing or definite spoofing
 * returns '1' if entry found or returns '0' if entry does not exists. 
 */
int verify_packet(struct sk_buff *skb)
{
    struct ethhdr *eh;
    struct iphdr *iph;
    unsigned char src_mac[ETH_ALEN];
    unsigned char spoofed_mac[ETH_ALEN];
    uint32_t src_ip;
    int i = 0;
    bool mac_found = 0;
    struct dhcp_struct *dhcp_entry_ptr;
    bool definite_spoof = 0;
    uint32_t spoofed_IP = 0;
    struct in_addr saddr_src, saddr_spoofed;
    char buf[512] = {'\0'};

	/* Extract source { MAC, IP }*/
	eh = eth_hdr(skb);
	memcpy(src_mac, eh->h_source, ETH_ALEN);
	iph = ip_hdr(skb);
	src_ip = (iph->saddr);
    saddr_src.s_addr = src_ip;
    
    /* Localhost packets should not get blocked*/
    if(src_ip == inet_addr("127.0.0.1")) { 
        return 1;
    }
     
    if (hash_empty(dhcp_snoop_table) == true) {
        /* The DHCP snoop table is empty */
        printk(KERN_DEBUG"IP Spoof Module:: Packet dropped MAC-IP association unknown MAC:%02X:%02X:%02X:%02X:%02X:%02X\t  IP:%s\n", src_mac[0], src_mac[1], src_mac[2], src_mac[3], src_mac[4], src_mac[5], inet_ntoa(saddr_src, buf, NULL));
        return 0;
    }
    else {
		struct dhcp_struct *ptr;
		/* Extract source { MAC, IP }*/
		eh = eth_hdr(skb);
		memcpy(src_mac, eh->h_source, ETH_ALEN);
		iph = ip_hdr(skb);
		src_ip = (iph->saddr);
        hash_for_each(dhcp_snoop_table, i, ptr, dhcp_hash_list) {
			/* check for MAC address */
			if((memcmp(&src_mac, &ptr->dhcp_mac, ETH_ALEN) == 0)) {
                /* check IP address*/
                /* If a satic entry of kind "AA:BB:CC:DD:EE:FF,.0.0.0.0" means
                 * AA:BB:CC:DD:EE:FF mac can appear with any IP address.
                 * This combination is considered as correct combination
                 * */
                mac_found = 1;
                dhcp_entry_ptr = ptr;
				if(src_ip == ptr->ip || ptr->ip == (uint32_t)0) {
                    //No need to LOG the correct combination.
					return 1;
				}
			}
		}
        /* Check for define spoof or possible spoof */
        /* Check: Is the IP matching with any IP in the DHCP snoop table*/
        if(mac_found == 1) {
            hash_for_each(dhcp_snoop_table, i, ptr, dhcp_hash_list) {
                if(dhcp_entry_ptr != ptr && src_ip == ptr->ip) {
                    /* Definite spoof */
                    spoofed_IP = ptr->ip;
                    memcpy(&spoofed_mac, ptr->dhcp_mac, ETH_ALEN);
                    definite_spoof = 1;
                    break;
                }
            }
            if(definite_spoof == 1) {
                /* This will be Definite spoof*/
                /* LOG about definite spoof */
                saddr_src.s_addr = src_ip;
                saddr_spoofed.s_addr = spoofed_IP;
                printk(KERN_DEBUG"IP Spoof Module:: Definite IP Spoof detected, Packet MAC:%02X:%02X:%02X:%02X:%02X:%02X\t  IP:%s, clashing with MAC:%02X:%02X:%02X:%02X:%02X:%02X in DHCP snoop table\n", src_mac[0], src_mac[1], src_mac[2], src_mac[3], src_mac[4], src_mac[5], inet_ntoa(saddr_src, buf, NULL), spoofed_mac[0], spoofed_mac[1], spoofed_mac[2], spoofed_mac[3], spoofed_mac[4], spoofed_mac[5]);
                return 0;
            }else if(definite_spoof == 0) {
                /* This will be Possible spoof*/
                /* LOG about possible spoof*/
                saddr_src.s_addr = src_ip;
                printk(KERN_DEBUG"IP Spoof Module:: Possible IP Spoof detected, Packet MAC:%02X:%02X:%02X:%02X:%02X:%02X\t  IP:%s\n", src_mac[0], src_mac[1], src_mac[2], src_mac[3], src_mac[4], src_mac[5], inet_ntoa(saddr_src, buf, NULL));
                printk(KERN_INFO"");
                return 0;
            }
        }
        /* If no entry is found then return 0----> meaning no entry found.*/
        /* LOG about Pkt dropped MAC:IP association unknown.*/

		/* Extract source { MAC, IP }*/
		eh = eth_hdr(skb);
		memcpy(src_mac, eh->h_source, ETH_ALEN);
		iph = ip_hdr(skb);
		src_ip = (iph->saddr);
        saddr_src.s_addr = src_ip;
        printk(KERN_DEBUG"IP Spoof Module:: Packet dropped MAC-IP association unknown MAC:%02X:%02X:%02X:%02X:%02X:%02X\t  IP:%s\n", src_mac[0], src_mac[1], src_mac[2], src_mac[3], src_mac[4], src_mac[5], inet_ntoa(saddr_src, buf, NULL));
		return 0;
	}
	return 0;
}

/**
 * Function to check if pkt is DHCP or not.
 * returns '1' if DHCP packet, '0' otherwise.
 */
int is_dhcp(struct sk_buff *skb)
{
    struct udphdr *udp_hdr = NULL;

    if(ip_hdr(skb)->protocol == IPPROTO_UDP) { 
        udp_hdr = (struct udphdr *)(skb->data + ((ip_hdr(skb)->ihl)<<2));

        if(udp_hdr->dest == htons(DHCPC) || udp_hdr->dest == htons(DHCPS) ||
            udp_hdr->source == htons(DHCPS) || udp_hdr->source == htons(DHCPC)) {
            return 1;
        }
	}
    return 0;
}
/**
 * Function to check if packet is DHCP release packet or not
 * Returns '1' if DHCP release pkt, '0' otherwise.
 */

int is_dhcp_release(struct sk_buff *skb)
{
    struct iphdr *iph=ip_hdr(skb);
    //struct ethhdr *mh = eth_hdr(skb);
    int offset=0;
    unsigned char *buff=skb->data;//pointer to the start of skb.
    unsigned char op,htype,hlen,hops;//dhcp field.
    unsigned char *hw;
    dhcp_pkt_type dhcp_type = 0;

    offset+= (int)(iph->ihl)*4;/* 4*ihl will be in bytes, ihl represented in 32 bit words */
    //This is dhcp packet /* Jagadeesh This selection is based on port number 0X43( 67 decimal) */
    offset+=8;//UDP header is 8-byte length
    op=buff[offset];
    htype=buff[offset+1];
    hlen=buff[offset+2];
    hops=buff[offset+3];

    //Message type+Hardware type+Hardware addr length+Hops
    //+Transaction ID+Seconds elapsed+Bootp flags+
    //+Client IP+Your IP+Next server IP+Relay agent IP
    offset+=1+1+1+1+4+2+2+4+4+4+4;//reference to RFC2131

    hw=buff+offset; /*hw is CHADDR*/
    //Client MAC address+Server host name+Boot file name+
    //Magic cookie+Option
    offset+=16+64+128+4+2;//reference to RFC2131 /*for +4+2 refer RFC2132*/

    dhcp_type=*(buff+offset);
    if (dhcp_type == DHCPRELEASE) {
        return 1;
    }

    return 0;
}

/*
 *
 * First Handle the DHCP server case
 * 1. Check pkt is DHCP ACK
 *      If yes just add to DHCP snooping table 
 *      If not Just accept.
 */
unsigned int dhcp_hook_function(unsigned int hooknum,
                                struct sk_buff *skb,
                                const struct net_device *in,
                                const struct net_device *out, 
                                int (*okfn) (struct sk_buff *))
{
    int ret = 0;
    struct iphdr *ip_header;

    /*If not Internet Protocol(IP) Packet Just Accept the packet*/
    if(skb->protocol != htons(ETH_P_IP)) {
    	return NF_ACCEPT;
    }

    /* If packet is IPv6, then just Accept the packet */
    ip_header = ip_hdr(skb);
    if(ip_header->version != IPV4) {
        return NF_ACCEPT;
    }

    ret = is_dhcp(skb);
    if(ret == 1) {
        	dhcp_process_packet(skb);
    }else {

    	return NF_ACCEPT;
    }
    return NF_ACCEPT;
}

/* sysfs related operations, store method just does nothing */
static ssize_t dhcp_show(struct kobject * kobj, struct kobj_attribute * attr, char * buf)
{
    return dump_list(buf);
}

static ssize_t dhcp_store(struct kobject * kobj, struct kobj_attribute * attr, const char * buf, size_t count)
{
    /**
     * Take input string and parse the string as comma as delimiter
     */
    unsigned char mac[ETH_ALEN] = {0};
    uint32_t ip;
    char *p;
    char *buffer;
    //struct in_addr addr;
    struct dhcp_struct *tmp;
    int i = 0;
    char delete_string[] = "delete";
    bool delete = 0;

    buffer = (char*) buf;
    //parse MAC address
    p = strsep(&buffer,",");
    if(!mac_pton(p, mac)) {
        printk(KERN_DEBUG"DHCP Snoop module::%s: Entered Invalid MAC address failed\n", __func__);
        return -EINVAL;
    }
    
    //parse IP address
    p = strsep(&buffer,",");
    /* If next string after MAC address is "delete" then delete the corresponding entry*/
    if(memcmp(p, delete_string, strlen(delete_string)) == 0){
        /* set the delete and use this flag in next hash_for_each*/
        delete = 1;
    }

    ip = inet_addr(p);
    /*Add to hash table */
    //Search in hash table 
    hash_for_each(dhcp_snoop_table, i, tmp, dhcp_hash_list) {
        // compare stored MAC address with PACKET mac address
        // dhcp_mac is client MAC that is snooped from previous DHCP transaction 
        // hw is CHADDR(client HW addr) field of present DHCP transaction
        if(memcmp(tmp->dhcp_mac, mac, ETH_ALEN) == 0) {
            /* If user sent "delete" instead of IP address, then delete the corresponfing entry*/
            if(delete == 1) {
                //printk(KERN_DEBUG"deleted entry");
                // LOG the entry deletion 
                log_dhcp_delete(tmp);

                hash_del(&tmp->dhcp_hash_list);
            }else {
                // If entry is found, replace the entry (or) update entry
                tmp->ip = ip;
                tmp->lease_time = 0;
                tmp->type = 0;
            }
            return count;
        }
    }
    // entry is not found, so add to hash table 
    struct dhcp_struct *static_entry = kmalloc(sizeof( struct dhcp_struct), GFP_KERNEL);
    if( static_entry == NULL ) {
        printk(KERN_DEBUG"DHCP Snoop module::%s: can't allocate memory\n",__func__);
        return count;           
    }
    /* If user has sent "delete" instead of IP address, then delete the corresponfing entry*/
    if(delete == 1) {
        /* There is no entry found. But user asked to delete the Entry.
         * Which is not possbible*/
        printk(KERN_DEBUG"DHCP snoop:: Tried to delete entry which does not exist\n");
        return count;
    }
    /* Otherwise add a new entry*/
    static_entry->ip = ip;
    memcpy(static_entry->dhcp_mac, mac, ETH_ALEN);
    static_entry->type = 0;// this is a static entry
    static_entry->lease_time = 0;
    // key value is last octate of IP address
    uint8_t key;
    key = (uint8_t) ip; /*take last octate as key*/
    hash_add(dhcp_snoop_table, &static_entry->dhcp_hash_list, key);
    /* LOG: Static entries should be logged. */
    struct in_addr saddr;
    char tmp_buffer[512] = {'\0'};
    saddr.s_addr = static_entry->ip;
    printk(KERN_DEBUG"DHCP Snoop module:: Static MAC-IP Entry added: IP:%s MAC:%02x:%02x:%02x:%02x:%02x:%02x \n ", inet_ntoa(saddr, tmp_buffer, NULL), static_entry->dhcp_mac[0], static_entry->dhcp_mac[1], static_entry->dhcp_mac[2], static_entry->dhcp_mac[3], static_entry->dhcp_mac[4], static_entry->dhcp_mac[5] );

    return count;
}

/*******************************************************************************/
/*********** sysfs for trusted interfaces ********************************/
static ssize_t trusted_interface_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    struct trusted_interface_list *ptr = trusted_if_head;
    int count = 0;

    /* Just traverse through the list and display*/
    if (trusted_if_head == NULL) {
        sprintf(buf, "No Trusted Interfaces");
        return count;
    }
    while(ptr != NULL) {
        count+=sprintf(buf+count, "%s \t", ptr->name);
        ptr = ptr->next;
    }
    count+=sprintf(buf+count,"\n"); /* This line is just for good alignment*/
    return count;
}

static struct net_device *if_to_netdev(const char *buffer)
{
	char *cp;
	char ifname[IFNAMSIZ + 2];

	if (buffer) {
		strlcpy(ifname, buffer, IFNAMSIZ);
		cp = ifname + strlen(ifname);
		while (--cp >= ifname && *cp == '\n')
			*cp = '\0';
		return dev_get_by_name(&init_net, ifname);
	}
	return NULL;
}

/* This function will LOG that a new trusted interface has been added*/
void log_added_new_trusted_interface(struct trusted_interface_list *iface)
{
   printk(KERN_DEBUG"DHCP snoop module:: Added a new trusted interface %s",iface->name);
}

/* Store method is handled correctly*/
static ssize_t trusted_interface_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
    /* add to the trusted interfaces list*/
    struct trusted_interface_list *ptr = trusted_if_head;
    struct trusted_interface_list *tmp;
    //char ifname[IFNAMSIZ] = {'\0'};
    struct net_device *dev;

    /* Sanity check of interface*/
    dev = if_to_netdev(buf);
    if(!dev) {
        printk(KERN_DEBUG"\n Invalid argument\n");
        return -EINVAL;
    }

    if (trusted_if_head == NULL) {
        tmp = kmalloc(sizeof(struct trusted_interface_list), GFP_KERNEL);
        if(!tmp) {
            printk(KERN_DEBUG"DHCP Snoop module::%s: can't allocate memory\n", __func__);
            return -ENOMEM;
        }
        /* How much need to copy: solved.... */
        memcpy(tmp->name, dev->name, IFNAMSIZ);
        tmp->name_len = strlen(dev->name);
        tmp->next = NULL;
        trusted_if_head = tmp;
        /* Generate a LOG that trusted interface is added by Admin*/
        log_added_new_trusted_interface(tmp);
        return count;
    }
    else {
        /*check if the interface is already in trusted ports list*/

        while(ptr != NULL) {
            if(memcmp(dev->name, ptr->name, ptr->name_len) == 0) {
                return count;
            }
            ptr = ptr->next;
        }

       tmp = kmalloc(sizeof(struct trusted_interface_list), GFP_KERNEL);
       if(!tmp) {
            printk(KERN_DEBUG"DHCP snoop module::%s: can't allocate memory\n", __func__);
            return -ENOMEM;
       }
       memcpy(tmp->name, dev->name, IFNAMSIZ);
       tmp->name_len = strlen(dev->name);
       /* Add new element at head*/
       tmp->next = trusted_if_head;
       trusted_if_head = tmp;
        /* Generate a LOG that trusted interface is added by Admin*/
        log_added_new_trusted_interface(tmp);
    }


    return count;
}
/**************** sysfs for dhcp snoop table *********************************/
static struct kobj_attribute dhcp_attribute     =
                    __ATTR(dhcp_snoop, 0644, dhcp_show, dhcp_store);
/********************** sysfs for trusted interfaces *************************/
static struct kobj_attribute trusted_interfaces = 
                    __ATTR(trusted_interfaces, 0644, trusted_interface_show, trusted_interface_store);


static struct attribute * attrs [] = {
    &dhcp_attribute.attr,
    &trusted_interfaces.attr,
    NULL,
};

static struct attribute_group attr_group = {
    .attrs = attrs,
};

static struct kobject *ex_kobj;

static void dhcp_timer_func(unsigned long data)
{
    dhcp_timer_task(0);
    tick_timer();
}


void tick_timer()
{
    dhcp_timer.expires = jiffies + delay * HZ;
    dhcp_timer.data=0;
    dhcp_timer.function = dhcp_timer_func;
    add_timer(&dhcp_timer);
}

/**
 *  IP Spoof
 * 	1. Register a hook for incoming packets.
 *	2. Get IP-MAC of packet and compare with DHCP snoop table.
 *	3. If Match, Accept the packet and no need to LOG.
 *	4. If not matched do print some diagnostic message.
 */
/**
 * If packet is coming on trusted interface, do not verify packet
 * If packet is coming on untrusted interface verify packet.
 * NF hook function, deals with only data packets(non DHCP packets)
 */
unsigned int data_hook_function(unsigned int hooknum, 
                                struct sk_buff *skb,
                				const struct net_device *in, 
				                const struct net_device *out, 
                				int (*okfn) (struct sk_buff*))
{
	int ret1 = 0;
    char buf[512] = {'\0'};
    unsigned char src_mac[ETH_ALEN];
	struct iphdr *ip_header;
    struct ethhdr *eh;
    struct in_addr saddr;
    struct trusted_interface_list *ptr = trusted_if_head;

	/* Check packet is Internet packet or not */
    if (skb->protocol != htons (ETH_P_IP)) {
		return NF_ACCEPT;
	}

    /* If packet is IPv6, then just Accept the packet  */
    ip_header = ip_hdr(skb);
    if (ip_header->version != IPV4) {
        return NF_ACCEPT;
    }

    /* There may be chance of DHCP packet, we should not drop the dhcp packet */
    /*
     */
    ret1 = is_dhcp(skb);
    if(ret1 == 1) {
        /* If packet is DHCP RELEASE, then LOG the RELEASE and delete  the corresponding entry*/
        int ret;
        
        ret = is_dhcp_release(skb);
        if(ret==1)
        {
            /* Log the DHCP Release IP, MAC*/
            struct dhcp_struct dhcp_struct;
            struct iphdr *iph=ip_hdr(skb);
            int offset = 0;
            unsigned char *buff=skb->data;//pointer to the start of skb.
            unsigned char *hw;
            dhcp_pkt_type dhcp_type = 0;
            uint8_t key;
            struct dhcp_struct *ptr;
            int i = 0;

            /* Get the clinet IP, MAC address it is different from client IP in DHCP ACK*/
            offset=0;
            offset+=(int)(iph->ihl)*4;//offset the IP header
            offset+=8;//offset the UDP header

            //Message type+Hardware type+Hardware addr length+Hops
            //+Transaction ID+Seconds elapsed+Bootp flags+
            offset+=1+1+1+1+4+2+2;//reference to RFC2131
            dhcp_struct.ip = *((uint32_t *)(buff+offset));
            
            // Client IP addr + Your IP addr + Next server IP addr + Relay Agent IP addr 
            offset+=4+4+4+4;
            hw = buff+offset;
            memcpy(dhcp_struct.dhcp_mac, hw, ETH_ALEN);
            
            dhcp_type = DHCPRELEASE;
            log_dhcp_transaction(&dhcp_struct, dhcp_type);

            /*Delete the corresponding entry*/
            key = (uint8_t)dhcp_struct.ip;
            hash_for_each(dhcp_snoop_table, i, ptr, dhcp_hash_list) {
                /* check for MAC address */
                if((memcmp(&dhcp_struct.dhcp_mac, &ptr->dhcp_mac, ETH_ALEN) == 0)) {

                /* check IP address*/
                    if(dhcp_struct.ip == ptr->ip) {
                        /* Found an entry  */
                        hash_del(&ptr->dhcp_hash_list);
                        kfree(ptr);
                    }
                }
            }

        }
        return NF_ACCEPT;
    }
    
    /*Check if packet is coming on trutsed interface ?*/
    eh = eth_hdr(skb);
    if(trusted_if_head != NULL) {
        //  there are trusted interfaces configured.
        while(ptr != NULL) {
            if(memcmp(in->name, ptr->name, ptr->name_len) == 0) {
                /// found in trusted list
                return NF_ACCEPT;
            }
            ptr = ptr->next;
        }
    }
    
    /* So, .....packet is not rxd on trusted interface, verify packet*/
	ret1 = verify_packet(skb);
	if (ret1 ==1) {
		//printk("IP-MAC relation is correct or some other condition has satisfied to accept the packet \n");
		return NF_ACCEPT;
	}
    else {
        if(preventive_mode == 1) {
            return NF_DROP;
        }else{
            return NF_ACCEPT;
        }
    }
	return NF_ACCEPT;
}

static struct nf_hook_ops dhcp_nfho;
static struct nf_hook_ops packet_nfho;

static int __init mod_init_func (void)
{
    int retval;

    /**
     * Use module parameter 'use_bridge' to decide the interface type to 
     * listen on.
     */

    /**
     * Hook positioned at POST_ROUTING will handle both the cases.. i.e 
     *  1. DHCP server is inside the box.
     *  2. DHCP server is out side the box.
     */
    dhcp_nfho.owner             = THIS_MODULE;
    dhcp_nfho.hook              = dhcp_hook_function;
    dhcp_nfho.hooknum           = NF_INET_POST_ROUTING;
    dhcp_nfho.priority          = NF_IP_PRI_FIRST;
    if(use_bridge == 1) {
        dhcp_nfho.pf            = PF_BRIDGE; // on bridge interface
    }
    else { 
        dhcp_nfho.pf            = PF_INET; // not on bridge interface
    }


    packet_nfho.owner      = THIS_MODULE;
    packet_nfho.hook       = data_hook_function;
    packet_nfho.hooknum    = NF_INET_PRE_ROUTING;
    packet_nfho.priority   = NF_IP_PRI_FIRST;
    if(use_bridge == 1) {
        packet_nfho.pf         = PF_BRIDGE; // on bridge interface
    }
    else {
        packet_nfho.pf         = PF_INET;
    }
    
    nf_register_hook(&dhcp_nfho);
    nf_register_hook(&packet_nfho);

    ex_kobj = kobject_create_and_add("dhcp", kernel_kobj);

    retval = sysfs_create_group(ex_kobj, &attr_group);
    if(retval)
        kobject_put(ex_kobj);

    init_timer_on_stack(&dhcp_timer);
    tick_timer();

    return 0;
}

static void __exit mod_exit_func (void)
{

    kobject_put(ex_kobj);
    del_timer(&dhcp_timer);

    nf_unregister_hook(&dhcp_nfho);
    nf_unregister_hook(&packet_nfho);

    clean_hash_table();
    clean_trusted_intrerfaces();
}

module_init (mod_init_func);
module_exit (mod_exit_func);
