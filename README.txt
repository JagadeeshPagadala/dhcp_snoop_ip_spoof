
DHCP snoop & IP spoof prevention module

Aim: To maintain DHCP snoop table (DHCP lease information) and to log and/or prevent IP spoofed packets.

IP spoof: If any uesr try to use an IP other than the IP it has acquired from DHCP server is called as IP spoofing.
	
Description:

	DHCP snooping:
		DHCP snoop and IP spoof is a single kernel space module written using Netfilter frame work.

	This module will be running on DHCP server and maintains DHCP lease information.
	One can see DHCP snoop table using following command 

	$cat /sys/kernel/dhcp/dhcp_snoop
	This table typically contains 
		IP			---- DHCP issued IP to the client
		MAC			---- MAC address of Client
		Lease Time	---- DHCP Lease time (Lease time will be '0' for static entries).
				
	Admin can enter a static entry using the following command
	$echo "MACADDRESS,IPaddress" > /sys/kernel/dhcp/dhcp_snoop
	As an example 
	$echo "00:0d:88:68:34:87,172.20.54.111" > /sys/kernel/dhcp/dhcp_snoop

    Special Case:
    Admin can configure a MAC to use any IP address using following command,
	$echo "MACADDRESS,0.0.0.0" > /sys/kernel/dhcp/dhcp_snoop
    As an example one can  add such an entry for MAC "00:0d:88:68:34:86"
	$echo "00:0d:88:68:34:86,0.0.0.0" > /sys/kernel/dhcp/dhcp_snoop
    Traffic coming from this MAC will not be checked for IP spoofing.

    Deleting DHCP snoop entry:
    Admin can delete a DHCP snoop table entry using following command line 
    $echo "MACADDRESS,delete" > /sys/kernel/dhcp/dhcp_snoop
    As an example to delete an entry which has MAC address "00:0d:88:68:34:86" 
    $echo "00:0d:88:68:34:86,delete" > /sys/kernel/dhcp/dhcp_snoop

    
	IP spoofing:
		IP spoofing will work in two modes preventive mode, monitor mode.
	Preventive mode:
		If IP spoofed packet is found then action will be Logging and dropping that packet.
		DHCP snoop table will be used as a referance for IP spoof check.
		If any packet doesn't match with an entery in DHCP snoop table(either static entry or entry learnt from DHCP ACK packet), then that packet will be logged 
		and DROPPED.

	Monitor mode:
		If IP spoofed packet is found then action will be Logging that packet.
		If any packet doesn't match with entery in DHCP snoop table(either static entry or learnt from DHCP     ACK packet), then that packet will be logged.
		
		One can choose IP spoofing mode of operation uisng the Kernel module parameter 'preventive_mode'.
		By default monitor mode will be active.
		Use following command to load the module in preventive mode.
		$insmod dhcp_snoop_IP_spoof.ko preventive_mode=1    
		
		Use following command to load the module in Monitor mode. (Default mode of operation)
		$insmod dhcp_snoop_IP_spoof.ko preventive_mode=0    


	Admin can define an interface as 'trusted interfaces' or 'untrusted interfaces'.

	Trusted interface:
		If we declare an interface as trusted interface then, any packet coming on that interface will not be checked for IP spoofing. However DHCP snoop table will have an entry of DHCP info happening on trusted interface.
	Untrusted interface:
		Any interface which is not trusted is untrusted interface.
		Any packet coming on the untrusted interface will gothrough the IP spoof check. If IP spoofed packet is found corresponing action will be taken based on mode of operation(preventive mode or monitor mode).

	One can define an interface as trusted interface using following command,
	$echo "Interface Name"  > /sys/kernel/dhcp/trusted_interface 
	As an example, to define eth0.120 as a trusted interafce use following command
	$echo "eth0.120"  > /sys/kernel/dhcp/trusted_interface 

	One can see all trusted interface defined using following command
	$cat /sys/kernel/dhcp/trusted_interface

Compilation:
    Follow the instuction in README.compile to compile the binary(ko file).



